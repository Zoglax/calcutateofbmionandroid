package com.example.calculatorofbmi;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import java.text.DecimalFormat;

public class MainActivity extends AppCompatActivity {

    EditText editTextWeight, editTextHeight, editTextAge, editTextResult;
    Spinner spinnerSex;
    Button buttonCalculate;

    String[] sex = new String[]{"man","woman"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextWeight = findViewById(R.id.editTextWeight);
        editTextHeight = findViewById(R.id.editTextHeight);
        editTextAge = findViewById(R.id.editTextAge);
        editTextResult = findViewById(R.id.editTextResult);

        spinnerSex = findViewById(R.id.spinnerSex);
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,sex);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerSex.setAdapter(spinnerAdapter);

        buttonCalculate = findViewById(R.id.buttonCalculate);
        buttonCalculate.setOnClickListener(buttonCalculateOnClick);
    }

    View.OnClickListener buttonCalculateOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v)
        {
            double weight = Double.parseDouble(editTextWeight.getText().toString());
            double height = Double.parseDouble(editTextHeight.getText().toString());
            double age = Double.parseDouble(editTextAge.getText().toString());

            int sex = spinnerSex.getSelectedItemPosition();

            height /=100.0;

            double result = weight/(Math.pow(height, 2));

            DecimalFormat format = new DecimalFormat("#.0");

            switch (sex)
            {
                case 0:
                    if (age>=19 && age <25)
                    {
                        if (result == 21.4)
                        {
                            editTextResult.setText("It's Normal: " + format.format(result) );
                        }
                        else if (result < 21.4)
                        {
                            editTextResult.setText("You have a lack of weight: " + format.format(result) );
                        }
                        else if (result > 21.4)
                        {
                            editTextResult.setText("You are overweight: " + format.format(result) );
                        }
                    }
                    if (age>=25 && age <35)
                    {
                        if (result == 21.6)
                        {
                            editTextResult.setText("It's Normal: " + format.format(result) );
                        }
                        else if (result < 21.6)
                        {
                            editTextResult.setText("You have a lack of weight: " + format.format(result) );
                        }
                        else if (result > 21.6)
                        {
                            editTextResult.setText("You are overweight: " + format.format(result) );
                        }
                    }
                    if (age>=35 && age <45)
                    {
                        if (result == 22.9)
                        {
                            editTextResult.setText("It's Normal: " + format.format(result) );
                        }
                        else if (result < 22.9)
                        {
                            editTextResult.setText("You have a lack of weight: " + format.format(result) );
                        }
                        else if (result > 22.9)
                        {
                            editTextResult.setText("You are overweight: " + format.format(result) );
                        }
                    }
                    if (age>=45 && age <55)
                    {
                        if (result == 25.8)
                        {
                            editTextResult.setText("It's Normal: " + format.format(result) );
                        }
                        else if (result < 25.8)
                        {
                            editTextResult.setText("You have a lack of weight: " + format.format(result) );
                        }
                        else if (result > 25.8)
                        {
                            editTextResult.setText("You are overweight: " + format.format(result) );
                        }
                    }
                    if (age>=55)
                    {
                        if (result == 26.6)
                        {
                            editTextResult.setText("It's Normal: " + format.format(result) );
                        }
                        else if (result < 26.6)
                        {
                            editTextResult.setText("You have a lack of weight: " + format.format(result) );
                        }
                        else if (result > 26.6)
                        {
                            editTextResult.setText("You are overweight: " + format.format(result) );
                        }
                    }
                    break;
                case 1:
                    if (age>=19 && age <25)
                    {
                        if (result == 19.5)
                        {
                            editTextResult.setText("It's Normal: " + format.format(result) );
                        }
                        else if (result < 19.5)
                        {
                            editTextResult.setText("You have a lack of weight: " + format.format(result) );
                        }
                        else if (result > 19.5)
                        {
                            editTextResult.setText("You are overweight: " + format.format(result) );
                        }
                    }
                    if (age>=25 && age <35)
                    {
                        if (result == 23.2)
                        {
                            editTextResult.setText("It's Normal: " + format.format(result) );
                        }
                        else if (result < 23.2)
                        {
                            editTextResult.setText("You have a lack of weight: " + format.format(result) );
                        }
                        else if (result > 23.2)
                        {
                            editTextResult.setText("You are overweight: " + format.format(result) );
                        }
                    }
                    if (age>=35 && age <45)
                    {
                        if (result == 23.4)
                        {
                            editTextResult.setText("It's Normal: " + format.format(result) );
                        }
                        else if (result < 23.4)
                        {
                            editTextResult.setText("You have a lack of weight: " + format.format(result) );
                        }
                        else if (result > 23.4)
                        {
                            editTextResult.setText("You are overweight: " + format.format(result) );
                        }
                    }
                    if (age>=45 && age <55)
                    {
                        if (result == 25.2)
                        {
                            editTextResult.setText("It's Normal: " + format.format(result) );
                        }
                        else if (result < 25.2)
                        {
                            editTextResult.setText("You have a lack of weight: " + format.format(result) );
                        }
                        else if (result > 25.2)
                        {
                            editTextResult.setText("You are overweight: " + format.format(result) );
                        }
                    }
                    if (age>=55)
                    {
                        if (result == 27.3)
                        {
                            editTextResult.setText("It's Normal: " + format.format(result) );
                        }
                        else if (result < 27.3)
                        {
                            editTextResult.setText("You have a lack of weight: " + format.format(result) );
                        }
                        else if (result > 27.3)
                        {
                            editTextResult.setText("You are overweight: " + format.format(result) );
                        }
                    }
                    break;
            }
        }
    };
}
